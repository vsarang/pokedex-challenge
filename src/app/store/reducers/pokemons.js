import _ from 'lodash';
import {
  FETCH_POKEMONS,
  FETCH_POKEMONS_DONE,
  LOAD_POKEMONS,
  LOAD_POKEMON
} from 'app/store/actions/pokemons';

const INITIAL_STATE = {
  all: [],
  byId: {},
  loading: false,
  nextPageUrl: 'https://pokeapi.co/api/v2/pokemon?limit=20&offset=0'
};

function pokemons(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_POKEMONS:
      return {
        ...state,
        loading: true
      };
    case FETCH_POKEMONS_DONE:
      return {
        ...state,
        loading: false
      };
    case LOAD_POKEMONS:
      return {
        ...state,
        all: _.concat(state.all, action.pokemons),
        nextPageUrl: action.nextPageUrl
      };
    case LOAD_POKEMON:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.pokemon.id]: action.pokemon
        }
      };
    default:
      return state;
  }
}

export default pokemons;
