import _ from 'lodash';

import { PokemonPropType } from 'app/components/pokemon';

const PokemonName = (props) => {
  const formattedNumber = '#' + _.padStart(props.pokemon.order, 3, '0');
  const formattedName = _.capitalize(props.pokemon.name);
  return `${formattedNumber} ${formattedName}`;
};

PokemonName.propTypes = {
  pokemon: PokemonPropType.isRequired
};

export default PokemonName;
