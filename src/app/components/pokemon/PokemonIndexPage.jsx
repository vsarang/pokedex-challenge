import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import Button from 'lib/components/inputs/button/Button';

import { fetchPokemons } from 'app/store/actions/pokemons';
import { saveCapturedPokemons, capturePokemon } from 'app/store/actions/capturedPokemons';
import { ANIMATION_DELAY } from 'app/components/App';
import { PokemonPropType } from 'app/components/pokemon';
import PokedexPage from 'app/components/pokedex/PokedexPage';
import PokemonCard from 'app/components/pokemon/PokemonCard';
import PokemonInfoCard from 'app/components/pokemon/PokemonInfoCard';
import PokeballSpinner from 'app/components/pokemon/PokeballSpinner';

import styles from './pokemon-index-page.scss';

const PokemonIndexPage = (props) => {
  const [initialized, setInitialized] = useState(_.some(props.pokemons));
  const [delayCards, setDelayCards] = useState(_.isEmpty(props.pokemons));
  const [selectedPokemon, setSelectedPokemon] = useState(null);
  const navigate = useNavigate();

  function fetchPokemons() {
    setDelayCards(true);
    return props.fetchPokemons();
  }

  function onScroll(event) {
    if (event.target.scrollHeight - event.target.scrollTop === event.target.offsetHeight) {
      fetchPokemons();
    }
  }

  function capturePokemon(capturedPokemon) {
    props.capturePokemon(capturedPokemon);
    props.saveCapturedPokemons();
  }

  useEffect(() => {
    if (_.isEmpty(props.pokemons)) {
      fetchPokemons().then(() => {
        _.delay(() => {
          setInitialized(true);
        }, ANIMATION_DELAY);
      });
    }
  }, []);

  return (
    <PokedexPage
      navbarRightContent={
        <Button
          large
          primary
          bold
          popIn
          imgSrc={ '/pokeball.png' }
          onClick={ () => navigate('/captured') }
        >
          { 'Captured Pokemons' }
        </Button>
      }
      sidebarContent={
        selectedPokemon && (
          <div className={ styles.sidebarContent }>
            <div className={ styles.cardWrapper }>
              <PokemonInfoCard
                squareCornersBottom
                pokemon={ selectedPokemon }
                onCapture={ capturePokemon }
              />
            </div>
          </div>
        )
      }
    >
      <div
        className={
          classNames(styles.pokemonIndex, {
            [styles.hasSidebar]: Boolean(selectedPokemon)
          })
        }
        onScroll={ onScroll }
      >
        {
          _.map(props.pokemons, (pokemon) => (
            <PokemonCard
              key={ pokemon.id }
              delay={ delayCards }
              pokemon={ pokemon }
              onClick={ setSelectedPokemon }
            />
          ))
        }
        <PokeballSpinner
          grow={ !initialized }
          hide={ !props.loading }
        />
      </div>
    </PokedexPage>
  );
};

PokemonIndexPage.propTypes = {
  loading: PropTypes.bool, // TODO: implement loading state
  pokemons: PropTypes.arrayOf(PokemonPropType).isRequired,
  fetchPokemons: PropTypes.func.isRequired,
  capturePokemon: PropTypes.func.isRequired,
  saveCapturedPokemons: PropTypes.func.isRequired
};

export default connect(
  (state) => ({
    loading: state.pokemons.loading,
    pokemons: state.pokemons.all
  }),
  {
    fetchPokemons,
    capturePokemon,
    saveCapturedPokemons
  }
)(PokemonIndexPage);
