import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

import styles from './pokedex-page.scss';

const PokedexPage = (props) => {
  return (
    <div className={ styles.page }>
      <div
        className={ styles.innerWrapper }
      >
        <div
          className={
            classNames(styles.header, {
              [styles.hasSidebar]: Boolean(props.sidebarContent)
            })
          }
        >
          <Link
            to={ '/' }
          >
            <div className={ styles.logoWrapper }>
              <img
                className={ styles.logo }
                src={ '/pokedex_logo.png' }
              />
            </div>
          </Link>
          { props.navbarRightContent }
        </div>
        <div className={ styles.content }>
          { props.children }
        </div>
      </div>
      {
        props.sidebarContent && (
          <div className={ styles.sidebar }>
            { props.sidebarContent }
          </div>
        )
      }
    </div>
  );
};

PokedexPage.propTypes = {
  navbarRightContent: PropTypes.node,
  sidebarContent: PropTypes.node,
  children: PropTypes.node
};

export default PokedexPage;
