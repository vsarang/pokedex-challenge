import moment from 'moment';
import PropTypes from 'prop-types';

const FormattedDate = (props) => {
  const date = moment(props.date, 'YYYY-MM-DD');
  return date.format('MMM D, YYYY');
};

FormattedDate.propTypes = {
  date: PropTypes.string.isRequired
};

export default FormattedDate;
