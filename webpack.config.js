const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = function({ env = 'development' } = {}) {
  const devMode = env === 'development';
  return {
    entry: './src/app/index.js',
    mode: devMode ? 'development' : 'production',
    module: {
      rules: [{
        test: /\.jsx?$/,
        include: path.join(__dirname, 'src'),
        use: 'babel-loader'
      }, {
        test: /\.scss$/,
        include: path.join(__dirname, 'src'),
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              camelCase: 'dashesOnly',
              sourceMap: false,
              localIdentName: devMode ?
                '[name]-[local]-[hash:base64:5]' :
                null
            }
          },
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: {
              additionalData: '@import \'app/components/index.scss\';',
              sassOptions: {
                includePaths: [
                  path.join(__dirname, 'src')
                ]
              }
            }
          }
        ]
      }, {
        test: /\.css$/,
        include: path.join(__dirname),
        use: [
          'style-loader',
          'css-loader'
        ]
      }]
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [
        path.join(__dirname, 'src'),
        'node_modules'
      ]
    },
    output: {
      path: path.resolve(__dirname, 'dist/'),
      publicPath: '/',
      filename: 'pokedex.[contenthash].js'
    },
    devServer: {
      contentBase: path.join(__dirname, 'dist/'),
      port: 8080,
      historyApiFallback: true
    },
    devtool: 'source-map',
    plugins: [
      new HtmlWebpackPlugin({
        template: path.join(__dirname, 'src/index.ejs'),
        title: 'Pokedex',
        description: 'Browse and capture pokemon!',
        themeColor: '#ffffff'
      }),
      new CopyPlugin({
        patterns: [{
          from: path.join(__dirname, 'public'),
          to: path.join(__dirname, 'dist/'),
          noErrorOnMissing: true
        }]
      })
    ]
  };
};
